var http = require('http');
var argv = process.argv.slice(2);
var port = process.env.PALLET_TEST_PORT || argv[0] || 8080;

// Run echo server
http.createServer(function(req, res){
	console.log(new Date, req.method, req.url)

	if (req.url === '/quit') {
		res.end('Bye');
		process.exit();
		return;
	}

	res.end('Ok');
}).listen(port);

console.log("Host started at localhost:%s", port);